import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by dcatalans on 26/05/16.
 */
public class Categoria {
    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public int getId_pare() {
        return id_pare;
    }

    public void setId_pare(int id_pare) {
        this.id_pare = id_pare;
    }

    private int id_categoria;
    private String descripcio;
    private int id_pare;

    public String toString(){
        return this.getDescripcio();
    }

}
