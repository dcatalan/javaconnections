import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.*;
import java.util.*;
import java.util.List;

/**
 * Created by dcatalans on 13/06/16.
 */
class DataBase {
    private static final String dbClassName = "com.mysql.jdbc.Driver";
    private Connection connection;

    public DataBase(String server, String username, String password) throws Exception {
        Class.forName(dbClassName);
        Properties p = new Properties();
        p.put("user", username);
        p.put("password", password);
        String connString = "jdbc:mysql://" + server + "/gestio_p";
        // Now try to connect
        connection = DriverManager.getConnection(connString, p);
    }


    public void insertProducte(Producte p) throws Exception{

        String sql = "SELECT descripcio FROM PRODUCTE WHERE descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, p.descripcio);
        ResultSet rs =preparedStatement.executeQuery();

        if(!rs.next()){
            sql = "SELECT id_tipus_iva, percentatge FROM TIPUS_IVA WHERE descripcio = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, p.tipus_iva.descripcio);
            rs = preparedStatement.executeQuery();

            if (rs.next()){
                p.tipus_iva.id_tipus = rs.getInt("id_tipus_iva");
                p.tipus_iva.percentatge = rs.getInt("percentatge");

                //Insereix categoria-producte
                int a = Programa.db.trobarIDCategoria(p.categoria.getDescripcio());
                if(a > 0) {
                    //Insereix Producte nou
                    sql = "INSERT INTO PRODUCTE SET descripcio = ?, tipus_IVA = ?, observacions=?";
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setString(1, p.descripcio);
                    preparedStatement.setInt(2, p.tipus_iva.id_tipus);
                    preparedStatement.setString(3, p.observacions);
                    preparedStatement.execute();

                    sql = "INSERT INTO PRODUCTE_CATEGORIA SET id_categoria = ?, id_producte = ?";
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, a);
                    preparedStatement.setInt(2, maxCodiProducte());
                    preparedStatement.execute();
                    throw new Exception("Producte Introduit");
                }
                else{
                    throw new Exception("Aquesta categoria no existeix");
                }
            }
            else {
                throw new Exception("No existeix aquest iva");
            }
        }
        else{
            throw new Exception("Ya existeix aquest producte");
        }
    }

    public void updateProducte(Producte p) throws Exception {
        int id_prod = trobarIDproducte(p.descripcio);
        String sql = "select id_producte from PRODUCTE where id_producte = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, id_prod);
        ResultSet rs = preparedStatement.executeQuery();

        if(rs.next()){
            sql = "UPDATE PRODUCTE SET descripcio = ?, observacions = ?, tipus_IVA = ? where id_producte = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,p.nom_canviar);
            preparedStatement.setString(2, p.observacions);
            preparedStatement.setInt(3, trobarIDtipusiva(p.tipus_iva.descripcio));
            preparedStatement.setInt(4, id_prod);
            preparedStatement.execute();
            throw new Exception("Producte Modificat");

        } else {
            throw new Exception("El producte introduit no existeix");
        }
    }

    public void cercarProducte(Producte p) throws Exception{
        String sql = "select descripcio, observacions, tipus_iva from PRODUCTE where descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, p.descripcio);
        ResultSet rs = preparedStatement.executeQuery();
        if(rs.next()){
            p.observacions = rs.getString("observacions");
            p.tipus_iva.id_tipus = rs.getInt("tipus_iva");

            sql = "select id_categoria from PRODUCTE_CATEGORIA where id_producte = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,trobarIDproducte(p.descripcio));
            rs = preparedStatement.executeQuery();
            rs.next();
            int id_categoria = rs.getInt("id_categoria");

            sql = "select descripcio from CATEGORIA where id_categoria = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id_categoria);
            rs = preparedStatement.executeQuery();
            rs.next();
            p.categoria.setDescripcio(rs.getString("descripcio"));

            sql = "select descripcio from TIPUS_IVA where id_tipus_iva = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,p.tipus_iva.id_tipus);
            rs = preparedStatement.executeQuery();
            rs.next();
            p.tipus_iva.descripcio = rs.getString("descripcio");
        }
        else {
            throw new Exception("El producte no existeix");
        }
    }

    public int trobarIDtipusiva(String nom) throws Exception{

        String sql = "SELECT id_tipus_iva from TIPUS_IVA WHERE descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, nom);
        ResultSet rs = preparedStatement.executeQuery();
        if(rs.next()) {
            int id = rs.getInt("id_tipus_iva");
            return id;
        }
        return 0;
    }

    public int trobarIDproducte(String nom) throws Exception{
    String sql = "SELECT id_producte from PRODUCTE WHERE descripcio = ?";
    PreparedStatement preparedStatement = connection.prepareStatement(sql);
    preparedStatement.setString(1, nom);
    ResultSet rs = preparedStatement.executeQuery();
    if(rs.next()) {
        int id = rs.getInt("id_producte");
        return id;
    }
    return 0;
}

    public int maxCodiProducte() throws Exception{
        String sql = "select max(id_producte) as max from PRODUCTE";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        int max = rs.getInt("max");
        return max;
    }

    public void deleteProducte(Producte p) throws Exception{
        String sql = "select id_producte from PRODUCTE where descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, p.descripcio);
        ResultSet rs = preparedStatement.executeQuery();

        if(rs.next()){
            sql = "delete from PRODUCTE where descripcio = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,p.descripcio);
            preparedStatement.execute();
        }
        else {
            System.out.println("El producte no existeix");
        }
    }

    public List<Tipus_IVA> selectAllTipusIva() throws SQLException, ClassNotFoundException {
        String sql = "Select id_tipus_iva,descripcio,percentatge from TIPUS_IVA";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet s =preparedStatement.executeQuery();

        ArrayList<Tipus_IVA> result = new ArrayList<>();
        while(s.next()){
            Tipus_IVA tiva = new Tipus_IVA();
            tiva.id_tipus = s.getInt("id_tipus_iva");
            tiva.descripcio = s.getString("descripcio");
            tiva.percentatge = s.getInt("percentatge");
            result.add(tiva);
        }
        return result;
    }

    public List<Categoria> selectAllCategoria() throws SQLException, ClassNotFoundException {
        String sql = "Select id_categoria, descripcio, id_pare from CATEGORIA";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet s =preparedStatement.executeQuery();

        ArrayList<Categoria> result = new ArrayList<>();
        while(s.next()){
            Categoria c = new Categoria();
            c.setId_categoria(s.getInt("id_categoria"));
            c.setDescripcio(s.getString("descripcio"));
            c.setId_pare(s.getInt("id_pare"));
            result.add(c);
        }
        return result;
    }

    public List<Producte> selectAllProductes() throws SQLException, ClassNotFoundException {
        String sql = "select p.descripcio,p.observacions,ti.descripcio as tipus_iva, c.descripcio as categoria from ((PRODUCTE as p inner join PRODUCTE_CATEGORIA as pc on \n" +
                "p.id_producte = pc.id_producte) inner join CATEGORIA as c on pc.id_categoria = c.id_categoria)\n" +
                "inner join TIPUS_IVA as ti on p.tipus_iva = ti.id_tipus_iva";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet s =preparedStatement.executeQuery();

        ArrayList<Producte> result = new ArrayList<>();
        while(s.next()){
            Producte p = new Producte();
            p.descripcio = s.getString("p.descripcio");
            p.observacions = (s.getString("observacions"));
            p.tipus_iva.descripcio = s.getString("tipus_iva");
            p.categoria.setDescripcio(s.getString("categoria"));
            result.add(p);
        }
        return result;
    }

    ///////////////////////////////////CATEGORIA//////////////////////////////////////
    public void visualitzaSubCategoria(Categoria c) throws SQLException, ClassNotFoundException {


        String sql = "select id_categoria, descripcio, id_pare from CATEGORIA where descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, c.getDescripcio());

        ResultSet s = preparedStatement.executeQuery();

        if (s.next()) {
            c.setId_categoria(s.getInt("id_categoria"));
            sql = "select c.id_categoria,c.descripcio, c2.id_categoria, " +
                    "c2.descripcio from CATEGORIA as c inner join CATEGORIA as c2 on " +
                    "c.id_pare = c2.id_categoria where c2.id_categoria = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,c.getId_categoria());


            s = preparedStatement.executeQuery();

            while (s.next()) {
                c.setDescripcio(s.getString("descripcio"));
                c.setId_pare(s.getInt("c2.id_categoria"));
            }


        }
        else {
            System.out.println("No existeix aquesta categoria");
        }

    }
    public void modificarCategoria(Categoria c) throws  SQLException, ClassNotFoundException {


        String sql = "select id_categoria, descripcio, id_pare from CATEGORIA where descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, c.getDescripcio());

        ResultSet s = preparedStatement.executeQuery();

        if (s.next()) {
            c.setId_categoria(s.getInt("id_categoria"));
            c.setDescripcio(s.getString("descripcio"));
            c.setId_pare(s.getInt("id_pare"));


            sql = "update CATEGORIA SET  WHERE id_categoria = ?";
            PreparedStatement preparedStatment =connection.prepareStatement(sql);
            preparedStatment.setInt(2, c.getId_categoria());
            preparedStatment.execute();
        }
        else {
            try {
                throw new Exception("La categoria no existeix");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void eliminarCategoria(Categoria c) throws  SQLException, ClassNotFoundException {

        String sql = "Select id_categoria, descripcio, id_pare from CATEGORIA where descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        preparedStatement.setString(1, c.getDescripcio());
        ResultSet s = preparedStatement.executeQuery();

        if(s.next()){
            c.setId_categoria(s.getInt("id_categoria"));
            sql = "select c.id_categoria,c.descripcio, c2.id_categoria, " +
                    "c2.descripcio from CATEGORIA as c inner join CATEGORIA as c2 on " +
                    "c.id_pare = c2.id_categoria where c2.id_categoria = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,c.getId_categoria());
            s = preparedStatement.executeQuery();

            if(!s.next()){
                sql = "delete from CATEGORIA where id_categoria = ?";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1,c.getId_categoria());
                preparedStatement.execute();
            }
            else{
                System.out.println("No es pot borrar aquesta categoria perque te referencies.");
            }

        }
        else{
            System.out.println("Aquesta categoria no existeix.");
        }
    }
    public void insertCategoria(Categoria c) throws SQLException, ClassNotFoundException{

        String sql = "select id_categoria, id_pare, descripcio from CATEGORIA where descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1,c.getDescripcio());
        ResultSet rs = preparedStatement.executeQuery();

        if (!rs.next()){
            System.out.println("La categoria pare no existeix");
            return;
        }
        c.setId_pare(rs.getInt("id_categoria"));


        sql ="select id_categoria, id_pare, descripcio from CATEGORIA where descripcio = ?";
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, c.getDescripcio());
        ResultSet s = preparedStatement.executeQuery();

        if(!s.next()){
            if(c.getId_pare() > 0) {
                sql = "INSERT INTO CATEGORIA SET descripcio = ?, id_pare = ?";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, c.getDescripcio());
                preparedStatement.setObject(2, c.getId_pare());

                preparedStatement.execute();
            }
            else {
                sql = "INSERT INTO CATEGORIA SET descripcio = ?";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, c.getDescripcio());

                preparedStatement.execute();
            }
        }
        else{
            System.out.println("Aquesta categoria ya existeix");
        }
    }

    public int trobarIDCategoria(String nom) throws Exception{

        String sql = "SELECT id_categoria from CATEGORIA WHERE descripcio = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, nom);
        ResultSet rs = preparedStatement.executeQuery();
        if(rs.next()) {
            int id = rs.getInt("id_categoria");
            return id;
        }
        return 0;
    }

}

class Programa {
    static DataBase db;
    public static void main(String[] args) throws Exception{
        // Falta llegir des de xml
        ArrayList param = Programa.importXMLConfig();
        db = new DataBase(param.get(0).toString(),param.get(1).toString(),param.get(2).toString());
        final JFrame frame = new JFrame("Producte");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton button_insert = new JButton("Insertar Producte");
        JButton button_modify = new JButton("Modificar Producte");
        JButton button_delete = new JButton("Borrar Producte");
        JButton button_select = new JButton("Visualitzar Productes");
        JButton button_export = new JButton("Exportar a base de dades");
        JButton button_import = new JButton("Importar a XML");

        frame.getContentPane().setLayout(new FlowLayout());
        frame.add(button_select);
        frame.add(button_insert);
        frame.add(button_modify);
        frame.add(button_delete);
        frame.add(button_export);
        frame.add(button_import);
        frame.pack();
        frame.setVisible(true);

        button_insert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ProducteInsertDialog pid = new ProducteInsertDialog(frame);
                pid.pack();
                pid.setVisible(true);
            }
        });
        button_modify.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ProducteModifyDialog pmd = new ProducteModifyDialog(frame);
                pmd.pack();
                pmd.setVisible(true);
            }
        });
        button_delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ProducteDeleteDialog pdd = new ProducteDeleteDialog(frame);
                pdd.pack();
                pdd.setVisible(true);
            }
        });

        button_select.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ProducteSelectDialog psd = new ProducteSelectDialog(frame);
                psd.pack();
                psd.setVisible(true);
            }
        });
        button_import.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    exportToXML();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        button_export.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    importFromXML();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public static ArrayList importXMLConfig() throws Exception{
        SimpleXML sxml = new SimpleXML(new FileInputStream("config.xml"));
        Document doc = sxml.getDoc();
        Element root = doc.getDocumentElement();
        ArrayList<String> result = new ArrayList<>();

        //Agafar tots els camps del XML de una persona
        result.add(sxml.getElement(root, "server").getTextContent());
        result.add(sxml.getElement(root, "username").getTextContent());
        result.add(sxml.getElement(root, "password").getTextContent());

        return result;
    }

    public static void exportToXML() throws Exception{
        SimpleXML simpleXML = new SimpleXML();
        Document doc = simpleXML.getDoc();

        //ROOT
        Element root = doc.createElement("data");
        doc.appendChild(root);

        for(Producte p: Programa.db.selectAllProductes()){
            //PRODUCTE-ARREL
            Element producte = doc.createElement("Producte");
            root.appendChild(producte);

            Element name = doc.createElement("name");
            name.setTextContent(p.descripcio);
            producte.appendChild(name);

            Element observ = doc.createElement("observacions");
            observ.setTextContent(p.observacions);
            producte.appendChild(observ);

            Element categoria = doc.createElement("categoria");
            categoria.setTextContent(p.categoria.getDescripcio());
            producte.appendChild(categoria);

            Element tipus_iva = doc.createElement("TipusIva");
            tipus_iva.setTextContent(p.tipus_iva.descripcio);
            producte.appendChild(tipus_iva);
        }

        FileOutputStream fos = new FileOutputStream(new File("dumb.xml"));
        simpleXML.write(fos);
    }

    public static void importFromXML() throws Exception{
        SimpleXML sxml = new SimpleXML(new FileInputStream("dumb.xml"));
        Document doc = sxml.getDoc();
        Element root = doc.getDocumentElement();
        List<Element> list = sxml.getChildElements(root);

        for(Element e: list ) {
            //Agafar tots els camps del XML de una persona
            Producte p = new Producte();
            p.descripcio = sxml.getElement(e, "name").getTextContent();
            p.observacions = sxml.getElement(e, "observacions").getTextContent();
            p.categoria.setDescripcio(sxml.getElement(e, "categoria").getTextContent());
            p.tipus_iva.descripcio =sxml.getElement(e,"TipusIva").getTextContent();

            Programa.db.insertProducte(p);
        }
    }
}