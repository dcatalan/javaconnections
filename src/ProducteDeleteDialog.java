import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class ProducteDeleteDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox tipusiva;
    private JTextField descripcio;
    private JTextArea observacions;
    private JButton cercar;
    private JButton delete;
    private JLabel result;
    private JComboBox categoria;

    public ProducteDeleteDialog(JFrame parent) {
        super(parent);
        setLocationRelativeTo(parent);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        DefaultComboBoxModel dcb = new DefaultComboBoxModel();
        DefaultComboBoxModel dcb1 = new DefaultComboBoxModel();

        try {
            for (Tipus_IVA t: Programa.db.selectAllTipusIva()){
                dcb.addElement(t);
            }

            for(Categoria c: Programa.db.selectAllCategoria()){
                dcb1.addElement(c);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        tipusiva.setModel(dcb);
        categoria.setModel(dcb1);

        tipusiva.setEnabled(false);
        categoria.setEnabled(false);
        observacions.setEditable(false);
        delete.setEnabled(false);
        final Producte p = new Producte();


        cercar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                p.descripcio = descripcio.getText();
                try {
                    Programa.db.cercarProducte(p);
                    categoria.getModel().setSelectedItem(p.categoria.getDescripcio());
                    observacions.setText(p.observacions);
                    tipusiva.getModel().setSelectedItem(p.tipus_iva.descripcio);
                    tipusiva.setEnabled(true);
                    categoria.setEnabled(true);
                    observacions.setEditable(true);
                    delete.setEnabled(true);

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        });

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    Programa.db.deleteProducte(p);
                    result.setText("Producte esborrat!");
                }catch (Exception e){
                    result.setText("Aquest producte no es por borrar!");
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });
    }

}
