import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class ProducteModifyDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox tipusiva;
    private JTextField descripcio;
    private JComboBox categoria;
    private JTextArea observacions;
    private JButton cercar;
    private JButton update;
    private JLabel result;

    public ProducteModifyDialog(JFrame parent) {
        super(parent);
        setLocationRelativeTo(parent);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        tipusiva.setEditable(false);
        categoria.setEditable(false);
        observacions.setEditable(false);
        update.setEnabled(false);

        DefaultComboBoxModel dcb = new DefaultComboBoxModel();
        DefaultComboBoxModel dcb1 = new DefaultComboBoxModel();

        try {
            for (Tipus_IVA t: Programa.db.selectAllTipusIva()){
                dcb.addElement(t);
            }

            for(Categoria c: Programa.db.selectAllCategoria()){
                dcb1.addElement(c);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        tipusiva.setModel(dcb);
        categoria.setModel(dcb1);

        final Producte p = new Producte();

        cercar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                p.descripcio = descripcio.getText();
                try {
                    Programa.db.cercarProducte(p);
                    tipusiva.getModel().setSelectedItem(p.tipus_iva.descripcio);
                    categoria.getModel().setSelectedItem(p.categoria.getDescripcio());
                    observacions.setText(p.observacions);
                    tipusiva.setEnabled(true);
                    categoria.setEnabled(true);
                    observacions.setEditable(true);
                    update.setEnabled(true);

                } catch (Exception e) {
                   result.setText(e.getMessage());
                }
            }
        });
        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    p.nom_canviar = descripcio.getText();
                    p.tipus_iva.descripcio = tipusiva.getSelectedItem().toString();
                    p.categoria.setDescripcio(categoria.getSelectedItem().toString());
                    p.observacions = observacions.getText();
                    Programa.db.updateProducte(p);
                    dispose();
                }catch (Exception e){
                    result.setText(e.getMessage());
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });

    }
}
