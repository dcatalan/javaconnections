import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class ProducteInsertDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox tipusiva;
    private JTextField descripcio;
    private JComboBox categoria;
    private JTextArea observacions;
    private JLabel resultLabel;

    public ProducteInsertDialog(JFrame parent) {
        setContentPane(contentPane);
        setLocationRelativeTo(parent);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        DefaultComboBoxModel dcb = new DefaultComboBoxModel();
        DefaultComboBoxModel dcb1 = new DefaultComboBoxModel();

        try {
            for (Tipus_IVA t: Programa.db.selectAllTipusIva()){
                dcb.addElement(t);
            }

            for(Categoria c: Programa.db.selectAllCategoria()){
                dcb1.addElement(c);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        tipusiva.setModel(dcb);
        categoria.setModel(dcb1);


        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    Producte p = new Producte();
                    p.descripcio = descripcio.getText();
                    p.observacions = observacions.getText();
                    Programa.db.insertProducte(p);
                    dispose();

                }catch (Exception e) {
                    resultLabel.setText(e.getMessage());
                }
            }
        });
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });
    }
}
